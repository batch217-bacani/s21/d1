// console.log("Hello World!");

// [SECTION] Arrays
// Arrays are used to store multiple related values in a single variable
// Usually we declare an array using square brackets ([]) also known as "Array Literals"

let studentNameA = '2020-1923';
let studentNameB = '2020-1924';
let studentNameC = '2020-1925';
let studentNameD = '2020-1926';

// Declaration of an Array
// SYNTAX --> let/cons arrayName = [elementA, elementB, elementC....];

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926'];

// Common example of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

// Possible use of an array but is not recommended
let mixedArr = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way of writing an array

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
]

// Create an array with values from variable
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);

// [SECTION] Length Property
// The .length property allows us to get and set the total number of items in an array.

let fullName = "Jamie Noble";
console.log(fullName.length);
console.log(cities.length);

// length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.

// We use .length-1 to delete the last item in an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// To delete a specific item in an array we can employ array methods (which will be shown in the next session) or an algorithm (set of code to process tasks).

// Another way to delete an item on an array
// Another example using decrementation (--)
cities.length--;
console.log(cities);

// We can't do same on string, however.
fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);





































// Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);

// You can also add it directly
console.log(bullsLegends[bullsLegends.length-1]);

// Adding items to an array
// Using indices, you can also add items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

// You can also add items at the end of the array. Instead of adding it in the front to avoid the risk of replacing the first items in the arry:
// newArr[newArr.length-1] = "Aerith Gainsborough"; - will overwrite the last item instead.

newArr[newArr.length] = "Barett Wallace";
console.log(newArr);

//Looping over an Array
// You can use a for loop to iterate over all items in an array
// Set the counter as the index and set the condition that as long as the current index iterated is less than the length of the array.

for(let index = 0; index < newArr.length; index++){
	//index = 0, < 3 --> T -> "Cloud Strife"
	//index = 1, < 3 --> T -> "Tifa Lockhart"
	//index = 2, < 3 --> T -> "Barrett Wallace"
	//You can use the loop counter as index to be able to show each array items in the console.
	console.log(newArr[index]);
}


let numArr = [5, 12, 30, 46, 40, 55, 69, 88];

for(let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}
	else{
		console.log(numArr[index] + " is not divisible by 5");
	}
	}